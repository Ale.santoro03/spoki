n=5
while n>0:
    print("numero: ", n,"fatto")
    n=n-1
print("\nfine")


for i in [1,2,3,4,5]: #print numbers between 1 and 5
    print (i)


#print all names with a for cycle
friends=['alex', 'samanta', 'lucia']
for friend in friends:
    print ('helllo', friend)
print("\ndone")

#find the smallest number
smallest = None
print("Before:", smallest)
for itervar in [3, 41, 12, 9, 74, 15]:
    if smallest is None or itervar < smallest:
        smallest = itervar
    print("Loop:", itervar, smallest)
print("\nSmallest:", smallest)



#find the largest number
largest=None
for large in [1,2,3,4,10,6,23,34,25]:
    if largest is None or large>largest:
        largest=large
print("\nthe largest is: ", largest)

#input and print
name = input("\nenter:")
print(name)

#print the length of a string
name="alessandro"
print("\nthe length of the name is: ",len(name))

#print the name with a for
name="alessandro"
for letter in name:
    print (letter)

name = "alessandro santoro"
print ("\nthe first 10 letters of name are: ",name[0:11])


if 'ale' in name:
    print("\nfound")

#replace a word in a string and print it to uppercase
greet= 'hello alessandro'
greet2=greet.replace('alessandro','maria')
print(greet2.upper())

#remove space befor and after in a string
ciao='    ciao alessandro    '
print("\n",ciao.lstrip())

lista=[1,2,3,4,5,6,7,8,9,23,24,21,26,19,14]
print(lista)
7 in lista #ritorna true

lista[1:3]#[2,3]
lista[:4]#[1,2,3,4]

lista.append(10)#add 10 at the end of the list
lista.sort()#order the list
print(lista)

#create a dictionary
dizionario={'alex':20, 'daniele':20, 'graziano':19}

#tuples
(x,y)=("alex",20)
print (x)

#REGULAR EXPRESSIONS
import re

#find all numeric characters
x='My 2 favourite numbers are:  8 and 6'
y=re.findall('[0-9]+',x)
print ("\n",y)

z=re.findall('^M+:')