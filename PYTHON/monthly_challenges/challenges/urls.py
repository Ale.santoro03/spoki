from django.urls import path

from . import views #importo il file views 

urlpatterns=[
    path('', views.index), # /challenges/     DEFAULT
    path('<int:month>',views.monthly_challenge_by_number),
    path('<str:month>', views.monthly_challenge,name="month-challenge") #con questa possiamo rimuovere le due precdenti perché è dinamica
]        

