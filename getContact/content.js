const finalArray = [];
let progressBar;
let progressTotal = 0;
let loadingText;

var userLang = navigator.language || navigator.userLanguage;

function setupSpokiLogo() {
    if (document.querySelector('header[data-testid="chatlist-header"] > div ~ div button[data-testid="spoki-logo-button"]')) {
        return;
    }

    const logoButton = document.createElement('button');
    logoButton.className = 'spoki-logo-button';
    logoButton.dataset.testid = 'spoki-logo-button';


    const imageElement = document.createElement('img');
    imageElement.className = 'image-element';
    imageElement.src = 'https://spoki.it/wp-content/uploads/2023/07/CloudDownload.svg';
    imageElement.width = 40;
    imageElement.height = 40;


    const buttonText = document.createElement('span');
    if (userLang == 'it-IT') {
        buttonText.textContent = 'Esporta contatti';
    } else if (userLang == 'es') {
        buttonText.textContent = 'Exportar contactos';
    } else {
        buttonText.textContent = 'Exports contacts';
    }

    buttonText.className = "button-text";

    logoButton.appendChild(imageElement);
    logoButton.appendChild(buttonText);
    logoButton.addEventListener('click', handleLogoClick);

    document.body.appendChild(logoButton);
}

function handleLogoClick() {
    openNextChat(0, 0);

    const coverDiv = document.createElement('div');
    coverDiv.className = "cover-div";

    const loadingImage = document.createElement('img');
    loadingImage.src = 'https://spoki.it/wp-content/uploads/2023/07/spoki-logo-primary-black.svg';
    loadingImage.className = "loading-image";
    loadingImage.style.top = 'calc(50% - 60px)';
    loadingImage.style.transform = 'translate(-50%, -50%)';

    coverDiv.appendChild(loadingImage);

    document.body.style.marginTop = '20px';
    // Adding the cover div to the document body
    document.body.appendChild(coverDiv);

    progressBar = document.createElement('div');
    underProgress = document.createElement('div');
    progressBar.className = "progress-bar";

    coverDiv.appendChild(progressBar);
    progressBar.appendChild(underProgress);


    loadingText = document.createElement('div');

    if (userLang == 'it-IT') {
        loadingText.textContent = 'Esportazione dei contatti in corso, non chiudere questa pagina fino al termine...';
    } else if (userLang == 'es') {
        loadingText.textContent = 'Exportando contactos, no cierre la página actual hasta el final...';
    } else {
        loadingText.textContent = 'Exporting contacts, do not close the current page until the end...';
    }

    loadingText.className = "loading-text";
    coverDiv.appendChild(loadingText);

    // Create a sub-div for black color filling
    const fillDiv = document.createElement('div');
    fillDiv.style.height = '100%';
    fillDiv.style.backgroundColor = '#15d36b';
    fillDiv.style.padding = '2px';
    underProgress.appendChild(fillDiv);

    //Function to update the filling width based on progress
    function updateProgress(percent) {
        fillDiv.style.width = `${percent}%`;
    }


}

function exportToCSV() {
    let csvData = 'first_name,last_name,phone\n';
    finalArray.sort((a, b) => {
        if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return -1;
        }
        if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return 1;
        }
        return 0;
    });
    finalArray.forEach((obj) => {
        const fullName = obj.name.replace(',', ''); // Remove comma from the name
        const [first_name, ...last_nameArr] = fullName.split(' ').filter(x => x); // Split the name into two parts
        const last_name = last_nameArr.join(' '); // Join the rest into last_name (in case there are spaces)
        const number = obj.num.replace(',', '');
        csvData += `${first_name},${last_name},${number}\n`; // Add csv line
    });

    //Create an "a" element and set the CSV data as "href" attribute
    const csvFile = new Blob([csvData], { type: 'text/csv' });
    const csvURL = URL.createObjectURL(csvFile);
    const link = document.createElement('a');
    link.href = csvURL;
    link.download = 'contacts.csv';
    link.style.display = 'none';

    // Add the 'a' element to the DOM 
    document.body.appendChild(link);
    link.click();

    // Clear and remove the 'a' element
    document.body.removeChild(link);
}

function isDuplicateObject(name, number) {
    //Verify is exist an object similar into the finalArray
    return finalArray.some((obj) => {
        return obj.name === name && obj.num === number;
    });
}

function logo() {
    setupSpokiLogo();
}

function openNextChat(currentChatIndex, page) {
    const chatButton = document.querySelector("header [data-testid='menu-bar-chat'] > div");
    chatButton.click();
    checkAndRun('[data-testid="drawer-left"] [data-testid="chat-list-search-container"] ~ div', () => {
        const contactList = document.querySelector('[data-testid="drawer-left"] [data-testid="chat-list-search-container"] ~ div');
        const pHeight = contactList.clientHeight;
        contactList.scroll(0, (pHeight - 144) * page);
        checkAndRun('[data-testid="cell-frame-container"] > div ~ div', () => {
            const chats = contactList.querySelectorAll("[data-testid='cell-frame-container'] > div ~ div");

            if (currentChatIndex >= chats.length) {

                const isLastPage = contactList.clientHeight + contactList.scrollTop == contactList.scrollHeight;
                if (isLastPage) {
                    exportToCSV();
                    if (userLang == 'it-IT') {
                        loadingText.textContent = "Esportazione effettuata con successo";
                    } else if (userLang == 'es') {
                        loadingText.textContent = "Exportación exitosa";
                    } else {
                        loadingText.textContent = "Export successfull";
                    }

                    return
                }
                checkAndRun("header [data-testid='menu-bar-chat'] > div", () => {
                    openNextChat(0, page + 1)
                });
            }

            const chat = chats[currentChatIndex];
            chat.click();
            checkAndRun('[data-testid="conversation-info-header-chat-title"]', () => {
                const info = document.querySelector('[data-testid="conversation-info-header-chat-title"]');
                info.click();
                checkAndRun('[data-testid="drawer-right"] section > div h2, [data-testid="contact-info-subtitle"], [data-testid="drawer-right"] section > div span[title]', () => {
                    let nameElement = document.querySelector('[data-testid="drawer-right"] section > div h2, [data-testid="contact-info-subtitle"], [data-testid="drawer-right"] section > div span[title]');
                    let numberElement = document.querySelector('[data-testid="drawer-right"] section > div h2 ~ div, [data-testid="section-about-and-phone-number"] ~ div[data-testid="container_with_separator"]');
                    const name = nameElement.textContent;
                    const number = numberElement.textContent;


                    if (!isDuplicateObject(name, number)) {
                        const obj = { name: name, num: number };
                        finalArray.push(obj);
                    }
                    console.log(finalArray);
                });
            });
            checkAndRun("header [data-testid='menu-bar-chat'] > div", () => {
                openNextChat(currentChatIndex + 1, page)
            });
        });

        if (!underProgress) {
            underProgress = document.createElement('div');
            underProgress.className = "under-progress";
            coverDiv.appendChild(underProgress);

        }


        // Calculate the progress percentage and update the progress bar width
        const scrollTop = contactList.scrollTop;
        const progressPercentage = (scrollTop / (contactList.scrollHeight - pHeight)) * 100;

        // Create a sub-div for black color filling
        underProgress.style.width = `${progressPercentage}%`;
    })

}

document.addEventListener("DOMContentLoaded", function () {
    checkAndRun("header [data-testid='menu-bar-chat'] > div", () => {
        //openNextChat(0, 0)
        logo();

        const linkElement = document.createElement('link');
        linkElement.rel = 'stylesheet';
        linkElement.href = 'style.css';

        document.head.appendChild(linkElement);
    })
});

function checkAndRun(selector, callback) {
    setTimeout(() => {
        if (document.querySelector(selector)) {
            return callback()
        }
        checkAndRun(selector, callback)
    }, 100)
}